definition module System._Posix

/**
 * POSIX system calls. Information about the meaning can be found your system's
 * manpages.
 */

from StdInt import IF_INT_64_OR_32
from System._Pointer import :: Pointer
from System.SysCall import class SysCallEnv

//* Test for read permission.
R_OK :== 4
//* Test for write permission.
W_OK :== 2
//* Test for execute permission.
X_OK :== 1
//* Test for existence.
F_OK :== 0

strerr :: !Int !*env -> *(!Pointer, !*env) | SysCallEnv env
unlink :: !{#Char} !*env -> (!Int, !*env) | SysCallEnv env special env=World
fork :: !*env -> (!Int, !*env) | SysCallEnv env special env=World
execvp :: !{#Char} !{#Pointer} !*env -> (!Int, !*env) | SysCallEnv env special env=World
waitpid :: !Int !{#Int} !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
wait :: !{#Int} !*env -> (!Int, !*env) | SysCallEnv env special env=World
exit :: !Int !*env -> *env | SysCallEnv env special env=World
getcwd :: !{#Char} !Int !*env -> (!Pointer, !*env) | SysCallEnv env special env=World
chdir :: !{#Char} !*env -> (!Int, !*env) | SysCallEnv env special env=World
mkdir :: !{#Char} !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
rmdir :: !{#Char} !*env -> (!Int, !*env) | SysCallEnv env special env=World
rename :: !{#Char} !{#Char} !*env -> (!Int, !*env) | SysCallEnv env special env=World
opendir :: !{#Char} !*env -> (!Pointer, !*env) | SysCallEnv env special env=World
closedir :: !Pointer !*env -> (!Int, !*env) | SysCallEnv env special env=World
readdir :: !Pointer !*env -> (!Pointer, !*env) | SysCallEnv env special env=World
pipe :: !Pointer !*env -> (!Int, !*env) | SysCallEnv env special env=World
getpid :: Int

posix_openpt :: !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
grantpt :: !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
unlockpt :: !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
ptsname :: !Int !*env -> (!Pointer, !*env) | SysCallEnv env special env=World
ttyname :: !Int !*env -> (!Pointer, !*env) | SysCallEnv env special env=World
open :: !Pointer !Int !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
//* Also calls open but with a string as an argument to save an allocation
opens :: !{#Char} !Int !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
tcgetattr :: !Int !Pointer !*env -> (!Int, !*env) | SysCallEnv env special env=World
cfmakeraw :: !Pointer !*env -> *env | SysCallEnv env special env=World
tcsetattr :: !Int !Int !Pointer !*env -> (!Int, !*env) | SysCallEnv env special env=World
setsid :: !*env -> *env | SysCallEnv env special env=World

malloc :: !Int !*env -> (!Pointer, !*env) | SysCallEnv env special env=World
free :: !Pointer !*env -> *env | SysCallEnv env special env=World
memcpy :: !Pointer !Pointer !Int !*env -> *env | SysCallEnv env special env=World
//* Variant that allows copying from a pointer to a Clean string
memcpyStringToPointer :: !Pointer !{#Char} !Int !*env -> *env | SysCallEnv env special env=World
//* Variant that allows copying from a Clean string to a pointer
memcpyPointerToString :: !{#Char} !Pointer !Int !*env -> *env | SysCallEnv env special env=World
memcmp :: !Pointer !Pointer !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World

dup2 :: !Int !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
close :: !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
ioctl :: !Int !Int !Pointer !*env -> (!Int, !*env) | SysCallEnv env special env=World
//* variant requiring an argument as third parameter
fcntlArg :: !Int !Int !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
read :: !Int !Pointer !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
write :: !Int !{#Char} !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
access :: !{#Char} !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
select_ :: !Int !Pointer !Pointer !Pointer !Pointer !*env -> (!Int, !*env) | SysCallEnv env special env=World
kill :: !Int !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World

timegm :: !{#Int} !*env -> *(!Int, !*env) | SysCallEnv env special env=World
//timegm is a pure C function so this is safe to do without an environment
timegm` :: !{#Int} -> Int
asctime :: !{#Int} !*env -> (!Pointer, !*env) | SysCallEnv env special env=World
ctime :: !{#Int} !*env -> *(!Pointer, !*env) | SysCallEnv env special env=World
clock :: !*env -> (!Int, !*env) | SysCallEnv env special env=World
time :: !Int !*env -> (!Int,!*env) | SysCallEnv env special env=World
gmtime :: !{#Int} !*env -> (!Pointer, !*env) | SysCallEnv env special env=World
mktime :: !{#Int} !*env -> (!Int, !*env) | SysCallEnv env special env=World
//strftime is a pure C function so this is safe to do without an environment
strftime` :: !{#Char} !Int !{#Char} !{#Int} !{#Char} -> (!Int,!{#Char})
localtime :: !{#Int} !*env -> (!Pointer, !*env) | SysCallEnv env special env=World
clock_gettime :: !Int !Pointer !*env -> (!Int, !*env) | SysCallEnv env special env=World
clock_gettime` :: !Int !{#Int} !*env -> (!Int, !*env) | SysCallEnv env special env=World

isatty :: !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
flock :: !Int !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World

realpath :: !String !String !*env -> (!Pointer, !*env) | SysCallEnv env special env=World

getenv :: !{#Char} !*env -> *(!Pointer, !*env) | SysCallEnv env special env=World
setenv :: !{#Char} !{#Char} !Int !*env -> (!Int, !*env) | SysCallEnv env special env=World
unsetenv :: !{#Char} !*env -> (!Int, !*env) | SysCallEnv env special env=World

nanosleep :: !{#Int} !Pointer !*env -> (!Int, !*env) | SysCallEnv env special env=World

gethostbyname :: !{#Char} !*env -> (!Pointer, !*env) | SysCallEnv env special env=World
