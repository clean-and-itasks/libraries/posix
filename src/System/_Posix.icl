implementation module System._Posix

import StdEnv
import System._Pointer
import System._Architecture
import System.SysCall

strerr :: !Int !*env -> *(!Pointer, !*env) | SysCallEnv env
strerr i world = _strerr i world
_strerr :: !Int !*env -> *(!Pointer, !*env)
_strerr i world = code {
	ccall strerror "I:p:A"
}

unlink :: !{#Char} !*env -> (!Int, !*env) | SysCallEnv env
unlink path world = _unlink path world
_unlink :: !{#Char} !*env -> (!Int, !*env)
_unlink path world = code {
	ccall unlink "s:I:A"
}
fork :: !*env -> (!Int, !*env) | SysCallEnv env
fork world = _fork world
_fork :: !*env -> (!Int, !*env)
_fork world = code {
	ccall fork ":I:A"
}
execvp :: !{#Char} !{#Pointer} !*env -> (!Int, !*env) | SysCallEnv env
execvp name argv world = _execvp name argv world
_execvp :: !{#Char} !{#Pointer} !*env -> (!Int, !*env)
_execvp name argv world = code {
	ccall execvp "sA:I:A"
}
waitpid :: !Int !{#Int} !Int !*env -> (!Int, !*env) | SysCallEnv env
waitpid pid status_p options world = _waitpid pid status_p options world
_waitpid :: !Int !{#Int} !Int !*env -> (!Int, !*env)
_waitpid pid status_p options world = code {
    ccall waitpid "IAI:I:A"
}

wait :: !{#Int} !*env -> (!Int, !*env) | SysCallEnv env
wait status_p env = _wait status_p env
_wait :: !{#Int} !*env -> (!Int, !*env)
_wait status_p env = code {
    ccall wait "A:I:A"
}

exit :: !Int !*env -> *env | SysCallEnv env
exit num world = _exit num world
_exit :: !Int !*env -> *env
_exit num world = code {
	ccall exit "I:V:A"
}
getcwd :: !{#Char} !Int !*env -> (!Pointer, !*env) | SysCallEnv env
getcwd buf size_t world = _getcwd buf size_t world
_getcwd :: !{#Char} !Int !*env -> (!Pointer, !*env)
_getcwd buf size_t world = code {
	ccall getcwd "sI:p:A"
}
chdir :: !{#Char} !*env -> (!Int, !*env) | SysCallEnv env
chdir name world = _chdir name world
_chdir :: !{#Char} !*env -> (!Int, !*env)
_chdir name world = code {
	ccall chdir "s:I:A"
}
mkdir :: !{#Char} !Int !*env -> (!Int, !*env) | SysCallEnv env
mkdir name mode world = _mkdir name mode world
_mkdir :: !{#Char} !Int !*env -> (!Int, !*env)
_mkdir name mode world = code {
	ccall mkdir "sI:I:A"
}
rmdir :: !{#Char} !*env -> (!Int, !*env) | SysCallEnv env
rmdir name world = _rmdir name world
_rmdir :: !{#Char} !*env -> (!Int, !*env)
_rmdir name world = code {
	ccall rmdir "s:I:A"
}
rename :: !{#Char} !{#Char} !*env -> (!Int, !*env) | SysCallEnv env
rename old new world = _rename old new world
_rename :: !{#Char} !{#Char} !*env -> (!Int, !*env)
_rename old new world = code {
	ccall rename "ss:I:A"
}
opendir :: !{#Char} !*env -> (!Pointer, !*env) | SysCallEnv env
opendir path world = _opendir path world
_opendir :: !{#Char} !*env -> (!Pointer, !*env)
_opendir path world = code {
	ccall opendir "s:p:A"
}
closedir :: !Pointer !*env -> (!Int, !*env) | SysCallEnv env
closedir dir world = _closedir dir world
_closedir :: !Pointer !*env -> (!Int, !*env)
_closedir dir world = code {
	ccall closedir "p:I:A"
}
readdir :: !Pointer !*env -> (!Pointer, !*env) | SysCallEnv env
readdir dir world = _readdir dir world
_readdir :: !Pointer !*env -> (!Pointer, !*env)
_readdir dir world = code {
	ccall readdir "p:p:A"
}
pipe :: !Pointer !*env -> (!Int, !*env) | SysCallEnv env
pipe arr world = _pipe arr world
_pipe :: !Pointer !*env -> (!Int, !*env)
_pipe arr world = code {
    ccall pipe "p:I:A"
}
getpid :: Int
getpid = code {
	ccall getpid ":I"
}
posix_openpt :: !Int !*env -> (!Int, !*env) | SysCallEnv env
posix_openpt a0 a1 = _posix_openpt a0 a1
_posix_openpt :: !Int !*env -> (!Int, !*env)
_posix_openpt a0 a1 = code {
	ccall posix_openpt "I:I:A"
}
grantpt :: !Int !*env -> (!Int, !*env) | SysCallEnv env
grantpt a0 a1 = _grantpt a0 a1
_grantpt :: !Int !*env -> (!Int, !*env)
_grantpt a0 a1 = code {
	ccall grantpt "I:I:A"
}
unlockpt :: !Int !*env -> (!Int, !*env) | SysCallEnv env
unlockpt a0 a1 = _unlockpt a0 a1
_unlockpt :: !Int !*env -> (!Int, !*env)
_unlockpt a0 a1 = code {
	ccall unlockpt "I:I:A"
}
ptsname :: !Int !*env -> (!Pointer, !*env) | SysCallEnv env
ptsname a0 a1 = _ptsname a0 a1
_ptsname :: !Int !*env -> (!Pointer, !*env)
_ptsname a0 a1 = code {
	ccall ptsname "I:p:A"
}
ttyname :: !Int !*env -> (!Pointer, !*env) | SysCallEnv env
ttyname a0 a1 = _ttyname a0 a1
_ttyname :: !Int !*env -> (!Pointer, !*env)
_ttyname a0 a1 = code {
	ccall ttyname "I:p:A"
}

open :: !Pointer !Int !Int !*env -> (!Int, !*env) | SysCallEnv env
open a0 a1 a2 a3 = _open a0 a1 a2 a3
_open :: !Pointer !Int !Int !*env -> (!Int, !*env)
_open a0 a1 a2 a3 = code {
	ccall open "pII:I:A"
}

//Special open for strings to save an allocation
opens :: !{#Char} !Int !Int !*env -> (!Int, !*env) | SysCallEnv env
opens p flags mode w = _opens p flags mode w
_opens :: !{#Char} !Int !Int !*env -> (!Int, !*env)
_opens p flags mode w = open p flags mode w
where
	open :: !{#Char} !Int !Int !*env -> (!Int, !*env)
	open a0 a1 a2 a3 = code {
		ccall open "sII:I:A"
	}

tcgetattr :: !Int !Pointer !*env -> (!Int, !*env) | SysCallEnv env
tcgetattr a0 a1 a2 = _tcgetattr a0 a1 a2
_tcgetattr :: !Int !Pointer !*env -> (!Int, !*env)
_tcgetattr a0 a1 a2 = code {
	ccall tcgetattr "Ip:I:A"
}
cfmakeraw :: !Pointer !*env -> *env | SysCallEnv env
cfmakeraw a0 a1 = _cfmakeraw a0 a1
_cfmakeraw :: !Pointer !*env -> *env
_cfmakeraw a0 a1 = code {
	ccall cfmakeraw "p:V:A"
}
tcsetattr :: !Int !Int !Pointer !*env -> (!Int, !*env) | SysCallEnv env
tcsetattr a0 a1 a2 a3 = _tcsetattr a0 a1 a2 a3
_tcsetattr :: !Int !Int !Pointer !*env -> (!Int, !*env)
_tcsetattr a0 a1 a2 a3 = code {
	ccall tcsetattr "IIp:I:A"
}
setsid :: !*env -> *env | SysCallEnv env
setsid w = _setsid w
_setsid :: !*env -> *env
_setsid w = code {
	ccall setsid ":V:A"
}

malloc :: !Int !*env -> (!Pointer, !*env) | SysCallEnv env
malloc num w = _malloc num w
_malloc :: !Int !*env -> (!Pointer, !*env)
_malloc num w = code {
	ccall malloc "I:p:A"
}

free :: !Pointer !*env -> *env | SysCallEnv env
free ptr world = _free ptr world
_free :: !Pointer !*env -> *env
_free ptr world = code {
   ccall free "p:V:A"
}

memcpy :: !Pointer !Pointer !Int !*env -> *env | SysCallEnv env
memcpy s p n w = _memcpy s p n w
_memcpy :: !Pointer !Pointer !Int !*env -> *env
_memcpy s p n w = code {
    ccall memcpy "ppI:V:A"
}

memcpyStringToPointer :: !Pointer !{#Char} !Int !*env -> *env | SysCallEnv env
memcpyStringToPointer p s n w = _memcpyStringToPointer p s n w
_memcpyStringToPointer :: !Pointer !{#Char} !Int !*env -> *env
_memcpyStringToPointer p s n w = code {
    ccall memcpy "psI:V:A"
}

memcpyPointerToString :: !{#Char} !Pointer !Int !*env -> *env | SysCallEnv env
memcpyPointerToString s p n w = _memcpyPointerToString s p n w
_memcpyPointerToString :: !{#Char} !Pointer !Int !*env -> *env
_memcpyPointerToString s p n w = code {
    ccall memcpy "spI:V:A"
}

memcmp :: !Pointer !Pointer !Int !*env -> (!Int, !*env) | SysCallEnv env
memcmp ptr1 ptr2 num w = _memcmp ptr1 ptr2 num w
_memcmp :: !Pointer !Pointer !Int !*env -> (!Int, !*env)
_memcmp ptr1 ptr2 num w = code {
	ccall memcmp "ppI:I:A"
}

dup2 :: !Int !Int !*env -> (!Int, !*env) | SysCallEnv env
dup2 old new world = _dup2 old new world
_dup2 :: !Int !Int !*env -> (!Int, !*env)
_dup2 old new world = code {
    ccall dup2 "II:I:A"
}
close :: !Int !*env -> (!Int, !*env) | SysCallEnv env
close fd world = _close fd world
_close :: !Int !*env -> (!Int, !*env)
_close fd world = code {
    ccall close "I:I:A"
}

ioctl :: !Int !Int !Pointer !*env -> (!Int, !*env) | SysCallEnv env
ioctl fd op ptr world = _ioctl fd op ptr world
_ioctl :: !Int !Int !Pointer !*env -> (!Int, !*env)
_ioctl fd op ptr world = code {
    ccall ioctl "IIp:I:A"
}

fcntlArg :: !Int !Int !Int !*env -> (!Int, !*env) | SysCallEnv env
fcntlArg fd op arg world = _fcntlArg fd op arg world
_fcntlArg :: !Int !Int !Int !*env -> (!Int, !*env)
_fcntlArg fd op arg world = code {
    ccall fcntl "III:I:A"
}

read :: !Int !Pointer !Int !*env -> (!Int, !*env) | SysCallEnv env
read fd buffer nBuffer world = _read fd buffer nBuffer world
_read :: !Int !Pointer !Int !*env -> (!Int, !*env)
_read fd buffer nBuffer world = code {
    ccall read "IpI:I:A"
}

write :: !Int !{#Char} !Int !*env -> (!Int, !*env) | SysCallEnv env
write fd buffer nBuffer world = _write fd buffer nBuffer world
_write :: !Int !{#Char} !Int !*env -> (!Int, !*env)
_write fd buffer nBuffer world = code {
    ccall write "IsI:I:A"
}

access :: !{#Char} !Int !*env -> (!Int, !*env) | SysCallEnv env
access path mode world = _access path mode world
_access:: !{#Char} !Int !*env -> (!Int, !*env)
_access path mode world = code {
	ccall access "sI:I:A"
}

select_ :: !Int !Pointer !Pointer !Pointer !Pointer !*env -> (!Int, !*env) | SysCallEnv env
select_ nfds readfds writefds exceptfds timeout world = _select_ nfds readfds writefds exceptfds timeout world
_select_ :: !Int !Pointer !Pointer !Pointer !Pointer !*env -> (!Int, !*env)
_select_ nfds readfds writefds exceptfds timeout world = code {
    ccall select "Ipppp:I:A"
}

kill :: !Int !Int !*env -> (!Int, !*env) | SysCallEnv env
kill pid sig world = _kill pid sig world
_kill :: !Int !Int !*env -> (!Int, !*env)
_kill pid sig world = code {
    ccall kill "II:I:A"
}

timegm :: !{#Int} !*env -> *(!Int, !*env) | SysCallEnv env
timegm tm w = _timegm tm w
_timegm :: !{#Int} !*env -> *(!Int, !*env)
_timegm tm w = code {
	ccall timegm "A:I:A"
}
timegm` :: !{#Int} -> Int
timegm` tm = code {
	ccall timegm "A:I"
}
asctime :: !{#Int} !*env -> (!Pointer, !*env) | SysCallEnv env
asctime a0 w = _asctime a0 w
_asctime :: !{#Int} !*env -> (!Pointer, !*env)
_asctime a0 w = code {
	ccall asctime "A:p:A"
}
ctime :: !{#Int} !*env -> *(!Pointer, !*env) | SysCallEnv env
ctime a0 w = _ctime a0 w
_ctime :: !{#Int} !*env -> *(!Pointer, !*env)
_ctime a0 w = code {
	ccall ctime "A:p:A"
}
clock :: !*env -> (!Int, !*env) | SysCallEnv env
clock world = _clock world
_clock :: !*env -> (!Int, !*env)
_clock world = code {
	ccall clock ":I:A"
}
time :: !Int !*env -> (!Int,!*env) | SysCallEnv env
time a0 world = _time a0 world
_time :: !Int !*env -> (!Int,!*env)
_time a0 world = code {
	ccall time "I:I:A"
}
gmtime :: !{#Int} !*env -> (!Pointer, !*env) | SysCallEnv env
gmtime a0 w = _gmtime a0 w
_gmtime :: !{#Int} !*env -> (!Pointer, !*env)
_gmtime a0 w = code {
	ccall gmtime "A:p:A"
}
mktime :: !{#Int} !*env -> (!Int, !*env) | SysCallEnv env
mktime tm world = _mktime tm world
_mktime :: !{#Int} !*env -> (!Int, !*env)
_mktime tm world = code {
	ccall mktime "A:I:I"
}
strftime` :: !{#Char} !Int !{#Char} !{#Int} !{#Char} -> (!Int,!{#Char})
strftime` a0 a1 a2 a3 a4 = code {
	ccall strftime "sIsA:I:A"
}
localtime :: !{#Int} !*env -> (!Pointer, !*env) | SysCallEnv env
localtime tm world = _localtime tm world
_localtime :: !{#Int} !*env -> (!Pointer, !*env)
_localtime tm world = code {
	ccall localtime "A:p:A"
}

clock_gettime :: !Int !Pointer !*env -> (!Int, !*env) | SysCallEnv env
clock_gettime id res w = _clock_gettime id res w
_clock_gettime :: !Int !Pointer !*env -> (!Int, !*env)
_clock_gettime id res w = code {
		ccall clock_gettime "Ip:I:A"
	}

clock_gettime` :: !Int !{#Int} !*env -> (!Int, !*env) | SysCallEnv env
clock_gettime` id res w = clock_gettime id res w
where
	clock_gettime :: !Int !{#Int} !*env -> (!Int, !*env)
	clock_gettime a0 a1 a2 = code {
		ccall clock_gettime "IA:I:A"
	}

isatty :: !Int !*env -> (!Int, !*env) | SysCallEnv env
isatty a0 a1 = _isatty a0 a1
_isatty :: !Int !*env -> (!Int, !*env)
_isatty a0 a1 = code {
	ccall isatty "I:I:A"
}

flock :: !Int !Int !*env -> (!Int, !*env) | SysCallEnv env
flock fd operation world = _flock fd operation world
_flock :: !Int !Int !*env -> (!Int, !*env)
_flock fd operation world = code {
    ccall flock "II:I:A"
}

realpath :: !String !String !*env -> (!Pointer, !*env) | SysCallEnv env
realpath path buf w = _realpath path buf w
_realpath :: !String !String !*env -> (!Pointer, !*env)
_realpath path buf w = code {
	ccall realpath "ss:p:A"
}

getenv :: !{#Char} !*env -> *(!Pointer, !*env) | SysCallEnv env
getenv a0 w = _getenv a0 w
_getenv :: !{#Char} !*env -> *(!Pointer, !*env)
_getenv a0 w = code {
	ccall getenv "s:p:A"
}

setenv :: !{#Char} !{#Char} !Int !*env -> (!Int, !*env) | SysCallEnv env
setenv a0 a1 a2 a3 = _setenv a0 a1 a2 a3
_setenv :: !{#Char} !{#Char} !Int !*env -> (!Int, !*env)
_setenv a0 a1 a2 a3 = code {
	ccall setenv "ssI:I:A"
}
unsetenv :: !{#Char} !*env -> (!Int, !*env) | SysCallEnv env
unsetenv a0 a1 = _unsetenv a0 a1
_unsetenv :: !{#Char} !*env -> (!Int, !*env)
_unsetenv a0 a1 = code {
	ccall unsetenv "s:I:A"
}

nanosleep :: !{#Int} !Pointer !*env -> (!Int, !*env) | SysCallEnv env
nanosleep req rem w = _nanosleep req rem w
_nanosleep :: !{#Int} !Pointer !*env -> (!Int, !*env)
_nanosleep req rem w = code {
		ccall nanosleep "Ap:I:A"
	}

gethostbyname :: !{#Char} !*env -> (!Pointer, !*env) | SysCallEnv env
gethostbyname a0 a1 = _gethostbyname a0 a1
_gethostbyname :: !{#Char} !*env -> (!Pointer, !*env)
_gethostbyname a0 a1 = code {
	ccall gethostbyname "s:p:p"
}
