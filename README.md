# posix

This library provides an interface to POSIX specific syscalls.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
