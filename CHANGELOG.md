# Changelog

### 1.2.0

- Feature: add `wait` system call.

#### 1.1.3

- Chore: support base `3.0`.

#### 1.1.2

- Feature: add `_Posix.access` and corresponding flags.

#### 1.1.1

- Fix: add helper functions for opendir and readdir (because of malformed code
  they were skipped previously).

### 1.1.0

- Fix: add helper functions for overloaded syscalls to fix the ccall behaviour.
- Fix: export `isatty`.
- Feature: add time related syscalls.

## 1.0.0

- Initial version, import modules from clean platform v0.3.34 and destill all
  posix system calls.
- Move syscalls from `Network._IP`, `System._Enviroment`, `System._FilePath`,
  `System._Time`, `System._Memory` modules to `System._Posix`
- Add class constraint to environment type variable to restrict types which can be used to perform syscalls with.
- Change memory related syscalls so that they all require to pass on a unique state.
